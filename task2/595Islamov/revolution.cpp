#include "./common/Mesh.hpp"
#include "./common/Application.hpp"
#include "./common/ShaderProgram.hpp"
#include "./common/Texture.hpp"
#include "./common/LightInfo.hpp"

#include <glm/simd/geometric.h>
#include <vector>
#include <algorithm>
#include <cmath>

class SurfaceApplication : public Application
{
public:
    MeshPtr _surface1;
    MeshPtr _surface2;
    MeshPtr _marker;

    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _textureShader;

    TexturePtr _texture1;
    TexturePtr _texture2;

    GLuint _sampler1;
    GLuint _sampler2;

    LightInfo _light;

    float _radius = 1.0;
    float _height = 3.0;

    float _lr = 10.0;
    float _phi = glm::pi<float>() * 0.25f;
    float _theta = 3.14 / 2;

    unsigned int N = 200;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _marker = makeSphere(0.1f);
        _surface1 = generateFirstSurface();
        _surface1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _surface2 = generateSecondSurface();
        _surface2->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //=========================================================
        //Инициализация шейдеров
        _markerShader = std::make_shared<ShaderProgram>("595IslamovData2/marker.vert", "595IslamovData2/marker.frag");
        _textureShader = std::make_shared<ShaderProgram>("595IslamovData2/texture.vert", "595IslamovData2/texture.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _texture1 = loadTexture("595IslamovData2/texture1.jpg");
        _texture2 = loadTexture("595IslamovData2/texture2.jpg");

        //=========================================================
        //Инициализация сэмплеров
        glGenSamplers(1, &_sampler1);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sampler2);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("surface", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, -10.0f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, 2.0f * glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _textureShader->use();

        _textureShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _textureShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _textureShader->setVec3Uniform("light.pos", lightPosCamSpace);
        _textureShader->setVec3Uniform("light.La", _light.ambient);
        _textureShader->setVec3Uniform("light.Ld", _light.diffuse);
        _textureShader->setVec3Uniform("light.Ls", _light.specular);

        float time = glfwGetTime();
        _textureShader->setFloatUniform("time", time/3);
        _textureShader->setFloatUniform("diff", 1);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler1);
        _texture1->bind();

        _textureShader->setIntUniform("diffuseTex", 0);
        _textureShader->setMat4Uniform("modelMatrix", _surface1->modelMatrix());
        _textureShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface1->modelMatrix()))));

        _surface1->draw();

        _texture1->unbind();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(1, _sampler2);
        _texture2->bind();

        _textureShader->setIntUniform("diffuseTex", 0);
        _textureShader->setMat4Uniform("modelMatrix", _surface2->modelMatrix());
        _textureShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface2->modelMatrix()))));

        _surface2->draw();

        //Рисуем маркер
        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(1, 0);
        glUseProgram(0);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateSurface(-2);
            } else if (key == GLFW_KEY_EQUAL) {
                updateSurface(2);
            }
        }
    }

private:
    glm::vec3 normal(float x, float y, float z, float x_n, float y_n, float z_n) {
        glm::vec3 start = glm::vec3(x_n, y_n, z_n);
        glm::vec3 end = glm::vec3(x, y, z);
        return normalize(glm::vec3(start.x - end.x, start.y - end.y, start.z - end.z));
    }

    MeshPtr generateFirstSurface()
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textures;

        for (size_t i = 0; i < N; ++i) {

            float curr_r = _radius * i * (2 * N - i) / N / N;
            float next_r = _radius * (i + 1) * (2 * N - i - 1) / N / N;

            float curr_h = _height * i / 2 / N;
            float next_h = _height * (i + 1) / 2 / N;

            for (size_t j = 0; j < N; ++j) {

                float phi = 2 * glm::pi<float>() * j / N;
                float theta = 2 * glm::pi<float>() * (j + 1) / N;

                float z = (curr_h + next_h) / 2;

                vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
                vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
                vertices.emplace_back(curr_r * cos(theta), curr_r * sin(theta), curr_h);

                normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
                normals.emplace_back(normal(curr_r * cos(theta), curr_r * sin(theta), curr_h, 0, 0, z));

                textures.emplace_back((float)j / N, (float)i / N);
                textures.emplace_back((float)(j + 1) / N, (float)(i + 1) / N);
                textures.emplace_back((float)j / N, (float)(i + 1) / N);

                vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
                vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
                vertices.emplace_back(next_r * cos(phi), next_r * sin(phi), next_h);

                normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(phi), next_r * sin(phi), next_h, 0, 0, z));

                textures.emplace_back((float)j / N, (float)i / N);
                textures.emplace_back((float)(j + 1) / N, (float)(i + 1) / N);
                textures.emplace_back((float)(j + 1) / N, (float)i / N);
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(textures.size() * sizeof(float) * 2, textures.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        return mesh;
    }

    MeshPtr generateSecondSurface()
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textures;

        for (size_t i = N; i < 2 * N; ++i) {

            float curr_r = _radius * i * (2 * N - i) / N / N;
            float next_r = _radius * (i + 1) * (2 * N - i - 1) / N / N;

            float curr_h = _height * i / 2 / N;
            float next_h = _height * (i + 1) / 2 / N;

            for (size_t j = 0; j < N; ++j) {

                float phi = 2 * glm::pi<float>() * j / N;
                float theta = 2 * glm::pi<float>() * (j + 1) / N;

                float z = (curr_h + next_h) / 2;

                vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
                vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
                vertices.emplace_back(curr_r * cos(theta), curr_r * sin(theta), curr_h);

                normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
                normals.emplace_back(normal(curr_r * cos(theta), curr_r * sin(theta), curr_h, 0, 0, z));

                textures.emplace_back((float)j / N, (float)i / N);
                textures.emplace_back((float)(j + 1) / N, (float)(i + 1) / N);
                textures.emplace_back((float)j / N, (float)(i + 1) / N);

                vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
                vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
                vertices.emplace_back(next_r * cos(phi), next_r * sin(phi), next_h);

                normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
                normals.emplace_back(normal(next_r * cos(phi), next_r * sin(phi), next_h, 0, 0, z));

                textures.emplace_back((float)j / N, (float)i / N);
                textures.emplace_back((float)(j + 1) / N, (float)(i + 1) / N);
                textures.emplace_back((float)(j + 1) / N, (float)i / N);
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(textures.size() * sizeof(float) * 2, textures.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        return mesh;
    }

    glm::vec3 calculateNormal(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        glm::vec3 result;

        glm::vec3 vec1(b[0]-a[0], b[1]-a[1], b[2]-a[2]);
        glm::vec3 vec2(c[0]-a[0], c[1]-a[1], c[2]-a[2]);

        result[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
        result[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
        result[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];

        if (a[2] > 0) {
            result[0] = -1 * result[0];
            result[1] = -1 * result[1];
            result[2] = -1 * result[2];
        }

        return normalize(result);
    }

    void updateSurface(int dN)
    {
        N += dN;

        _surface1 = generateFirstSurface();
        _surface1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _surface2 = generateSecondSurface();
        _surface2->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        draw();
    }
};

int main() {
    SurfaceApplication app;
    app.start();
    return 0;
}
