set(SRC_FILES
	main.cpp
    common/Application.cpp
		common/DebugOutput.cpp
		common/Camera.cpp
		common/Mesh.cpp
		common/ShaderProgram.cpp
)
set(HEADER_FILES
		common/Application.hpp
		common/DebugOutput.h
		common/Camera.hpp
		common/Mesh.hpp
		common/ShaderProgram.hpp
)
include_directories("common")

MAKE_OPENGL_TASK(594Popovkin 1 "${SRC_FILES}")
