#include <assert.h>
#include "Maze.hpp"


Maze::Maze(std::string filepath)
{
    std::ifstream mazeFile(filepath.c_str());
    assert( !mazeFile.fail());
    unsigned long  height, width;
    mazeFile >> height >> width;
    assert( height > 0 &&
            height < 1000 &&
            width > 0 &&
            width < 1000 );

    mazeFile >> startXPosition >> startYPosition;
    assert( startXPosition > 0 &&
            startXPosition < height &&
            startYPosition > 0 &&
            startYPosition < width);

    mazeCells = std::vector<std::vector<bool> >(height, std::vector<bool>(width));
    char c;
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            mazeFile >> c;
            mazeCells[i][j] = ( c == '#' );
        }
    }
    mazeFile.close();
}

std::vector<bool> &Maze::operator[](int idx)
{
    return mazeCells[idx];
}

const std::vector<bool> &Maze::operator[](int idx) const
{
    return mazeCells[idx];
}

unsigned long Maze::GetHeight() const
{
    return mazeCells.size();
}

unsigned long Maze::GetWidth() const
{
    assert(GetHeight() > 0);
    return mazeCells[0].size();
}
