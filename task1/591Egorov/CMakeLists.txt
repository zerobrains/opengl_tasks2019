# define GLM_ENABLE_EXPERIMENTAL to use glm/gtx/transform.hpp

add_definitions(-D GLM_ENABLE_EXPERIMENTAL)

set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/DebugOutput.cpp
    common/ShaderProgram.cpp
    common/Mesh.cpp
    Main.cpp
    MazeCameraMover.cpp
    Maze.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/Camera.hpp
        common/Mesh.hpp

    Maze.hpp
)

MAKE_OPENGL_TASK(591Egorov 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(591Egorov1 stdc++fs)
endif()

target_include_directories(591Egorov1 PUBLIC
        common)
